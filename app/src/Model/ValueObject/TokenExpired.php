<?php

declare(strict_types=1);

namespace App\Model\Link\ValueObject;

use App\Exception\InvalidTokenExpiredException;
use App\Exception\InvalidUrlException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class TokenExpired
{
    /**
     * @ORM\Column(type="string")
     */
    private $token_expired;

    public function __construct(string $tokenExpired)
    {
        if (!filter_var($tokenExpired, FILTER_VALIDATE_INT)) {
            throw new InvalidTokenExpiredException(sprintf('%s - not a valid token lifetime value', $tokenExpired));
        }

        $minutes = $tokenExpired;
        $time = new \DateTime();
        $time->add(new \DateInterval('PT' . $minutes . 'M'));
        $stamp = $time->format('Y-m-d H:i:s');

        $this->token_expired = $stamp;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->token_expired;
    }
}