<?php

declare(strict_types=1);

namespace App\Model\Link\Entity;

use App\Model\Link\ValueObject\Token;
use App\Model\Link\ValueObject\TokenExpired;
use App\Model\Link\ValueObject\Url;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinkRepository")
 */
final class Link
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true, name="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     */
    private $uuid;

    /**
     * @var Url
     *
     * @ORM\Embedded(class="App\Model\Link\ValueObject\Url", columnPrefix=false)
     */
    private $url;

    /**
     * @var Token
     *
     * @ORM\Embedded(class="App\Model\Link\ValueObject\Token", columnPrefix=false)
     */
    private $token;

    /**
     * @var TokenExpired
     *
     * @ORM\Embedded(class="App\Model\Link\ValueObject\TokenExpired", columnPrefix=false)
     */
    private $token_expired;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint")
     */
    private $views;

    /**
     * @var json
     *
     * @ORM\Column(type="json")
     */
    private $headers;

    /**
     * @param Url $url
     * @param Token $token
     * @param TokenExpired $token_expired
     *
     * @throws Exception
     */
    public function __construct(
        Url $url,
        Token $token,
        TokenExpired $token_expired
    ) {
        $this->uuid = Uuid::uuid4();
        $this->url = $url;
        $this->token = $token;
        $this->token_expired = $token_expired;
        $this->created_at = new DateTime();
        $this->views = 0;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @return Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }

    /**
     * @return TokenExpired
     */
    public function getTokenExpired(): TokenExpired
    {
        return $this->token_expired;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updated_at;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return (int)$this->views;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers ?? [];
    }
}