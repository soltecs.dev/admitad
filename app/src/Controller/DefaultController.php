<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use App\Entity\Items;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{

    public function index()
    {
        return $this->render('home.html.twig');
    }

    public function getCategoryMaxPrices()
    {
        $items = $this->getDoctrine()->getRepository(Items::class)->getCategoryMaxPrices();

        return $this->render('categoryPrices.html.twig', array('items' => $items));
    }

    public function getUserStats()
    {
        $arResult = [];
        $items = $this->getDoctrine()->getRepository(Users::class)->getUserStats();

        foreach($items as $item) {
            $arResult[$item['year']][$item['gender']] = $item['count'];
        }

        return $this->render('userStats.html.twig', array('items' => $arResult));
    }
}