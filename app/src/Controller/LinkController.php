<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Users;
use App\Model\Link\Entity\Link;
use App\Model\Link\ValueObject\Token;
use App\Model\Link\ValueObject\TokenExpired;
use App\Model\Link\ValueObject\Url;
use App\Repository\LinkRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class LinkController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param EntityManagerInterface $em
     * @param LinkRepository $linkRepository
     * @param Environment $twig
     * @param RouterInterface $router
     */
    public function __construct(
        EntityManagerInterface $em,
        LinkRepository $linkRepository,
        Environment $twig,
        RouterInterface $router
    )
    {
        $this->em = $em;
        $this->linkRepository = $linkRepository;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     *
     * @Route("/short", name="view_links")
     */
    public function viewLinks()
    {
        return new Response($this->twig->render('links.html.twig', [
            'stats' => $this->getDoctrine()->getRepository(Link::class)->getStats()
        ]));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     *
     * @throws NonUniqueResultException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     *
     * @Route("/links/generate", name="generate_links")
     */
    public function createLink(Request $request)
    {
        if (!$request->get('url')) {
            return new RedirectResponse($this->router->generate('view_links'));
        }

        $stats = $this->getDoctrine()->getRepository(Link::class)->getStats();

        $url = new Url($request->get('url'));
        $tokenExpired = new TokenExpired($request->get('token_expired'));

        /** @var Link $existsLink */
        $existsLink = $this->linkRepository->findLinkByUrl($url);

        if (!$existsLink) {
            $link = new Link($url, new Token(), $tokenExpired);

            $this->em->persist($link);
            $this->em->flush();
        }

        return new RedirectResponse($this->router->generate('view_links'));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws DBALException
     *
     * @Route("/short/{token}", name="redirect")
     */
    public function redirectToSite(Request $request)
    {
        /** @var Link $link */
        $link = $this->linkRepository->findUrlByToken($request->get('token'));

        if ($link === null) {
            return new RedirectResponse('http://127.0.0.1:8080');
        }

        $this->linkRepository->updateViews($link->getUrl(), $link->getTokenExpired());

        return new RedirectResponse($link->getUrl()->getValue());
    }
}