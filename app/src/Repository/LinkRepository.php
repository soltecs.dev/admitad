<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\Link\Entity\Link;
use App\Model\Link\ValueObject\TokenExpired;
use App\Model\Link\ValueObject\Url;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpClient\HttpClient;

/**
 * @method Link|null find($id, $lockMode = null, $lockVersion = null)
 * @method Link|null findOneBy(array $criteria, array $orderBy = null)
 * @method Link[]    findAll()
 * @method Link[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    /**
     * @param Url $url
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findLinkByUrl(Url $url)
    {
        return $this
            ->createQueryBuilder('link')
            ->where('link.url.url = :url')
            ->setParameter('url', $url->getValue())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $token
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findUrlByToken(string $token)
    {
        return $this
            ->createQueryBuilder('link')
            ->where('link.token.token = :token')
            ->andWhere('link.token_expired.token_expired > :current_date')
            ->setParameter('token', $token)
            ->setParameter('current_date', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getClientHeaders()
    {
        $arResult = [
            'country' => '',
            'city' => '',
            'user_agent' => '',
        ];

        $client = HttpClient::create(['headers' => [
            'User-Agent' => 'My Fancy App',
        ]]);

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $response = $client->request('GET', 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/iplocate/address?ip=' . $ip, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Token 9f99f6825cd0d6bdc87750ec6fbc627444d27ab9',
            ],
        ]);

        $statusCode = $response->getStatusCode();

        if ($statusCode == 200) {
            $content = $response->toArray();
            $country = $content['location']['data']['country'];
            $city = $content['location']['value'];
            $userAgent = $_SERVER['HTTP_USER_AGENT'];

            $arResult = [
                'country' => $country,
                'city' => $city,
                'user_agent' => $userAgent,
            ];
        }

        return $arResult;
    }

    /**
     * @param $url
     * @param $tokenExpired
     *
     * @throws DBALException
     */
    public function updateViews(Url $url, TokenExpired $tokenExpired)
    {
        $sql = '
            UPDATE link
            SET views = views + 1, headers=:headers, token_expired=:token_expired
            WHERE url = :url
        ';

        $this->getEntityManager()->getConnection()->executeUpdate($sql, [
            'url' => $url->getValue(),
            'headers' => json_encode(self::getClientHeaders()),
            'token_expired' => $tokenExpired->getValue(),
        ]);
    }

    public function getStats(): array
    {
        $arResult = [];

        $links = $this->findAll();

        foreach ($links as $link) {
            $arResult[] = [
                'token' => $link->getToken()->getValue(),
                'token_expired' => $link->getTokenExpired()->getValue(),
                'target' => $link->getUrl()->getValue(),
                'views' => $link->getViews(),
                'headers' => $link->getHeaders(),
                'updated_at' => $link->getCreatedAt()
            ];
        }

        return $arResult;
    }
}